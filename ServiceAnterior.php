<?php

	if(isset($_GET['wsdl'])){
		$mysqli = new mysqli('localhost', 'root', '', 'arboles_vectorez');
		if ($mysqli->connect_errno) {
		    // La conexión falló. ¿Que vamos a hacer?
		    // Se podría contactar con uno mismo (¿email?), registrar el error, mostrar una bonita página, etc.
		    // No se debe revelar información delicada

		    // Probemos esto:
		    echo "Lo sentimos, este sitio web está experimentando problemas.";

		    //Algo que no se debería de hacer en un sitio público, aunque este ejemplo lo mostrará
		    //de todas formas, es imprimir información relacionada con errores de MySQL -- se podría registrar
		    //echo "Error: Fallo al conectarse a MySQL debido a: \n";
		    //echo "Errno: " . $mysqli->connect_errno . "\n";
		    //echo "Error: " . $mysqli->connect_error . "\n";

		    // Podría ser conveniente mostrar algo interesante, aunque nosotros simplemente saldremos
		    exit;
		}

		$mysqli->query("SET NAMES 'utf8'");
		if($_GET['wsdl'] == 'InsertArbol'){

			$Sector = $_GET['sector'];
			$Lote  = $_GET['Lote'];
			if($_GET['id_web'] != 0){
				$Lsql = "UPDATE Info_arboles SET codigo_qr = " . $_GET['codigo_qr'] . " , sector = " . $Sector . " , lote = ". $Lote . " , numero_arbol = '". $_GET['numero_arbol'] . "' , diametro = '" . $_GET['diametro'] . "' , tipo_diametro = '" . $_GET['tipo_diametro'] . "' , cap = '" . $_GET['cap'] . "' , calidad =  '". $_GET['calidad'] . "' , fecha = '". $_GET['fecha'] . "' , usuario_creador =  ". $_GET['Usuario'] . " , imei_celular ='".$_GET['imei']."' WHERE id = " . $_GET['id_web'];

			} else{
				$Lsql = "INSERT INTO Info_arboles (codigo_qr, sector, lote, numero_arbol, diametro, tipo_diametro, cap, calidad, estado, fecha, usuario_creador, imei_celular) VALUES (" . $_GET['codigo_qr'] . " , " . $Sector . " , ". $Lote . " , '". $_GET['numero_arbol'] . "' , '" . $_GET['diametro'] . "' , '". $_GET['tipo_diametro'] ."' , '" . $_GET['cap'] . "' , '". $_GET['calidad'] . "' , 1 , '". $_GET['fecha'] . "' , ". $_GET['Usuario'] . " , '".$_GET['imei']."') ";
			}

			$json = array();
			//echo $Lsql;
			if ($mysqli->query($Lsql) === TRUE) {
				if($mysqli->insert_id != 0){
					$Ysql = "INSERT INTO relacion_equipos_registros (rel_imei, rel_reg_id) VALUES ('".$_GET['imei']."' , ".$mysqli->insert_id.")";
					$mysqli->query($Ysql);
				}

				$json['code'] = 1;
				if($_GET['id_web'] != 0){
					$json['id'] = $_GET['id_web'];
					$Ysql = "DELETE FROM relacion_equipos_registros WHERE  rel_reg_id = ".$_GET["id_web"];
					$mysqli->query($Ysql);
				}else{
					$json['id'] = $mysqli->insert_id;
				}
				$json['message'] = 'Exito';
				echo json_encode($json);
			} else {
				$json['code'] = 0;
				$json['id'] = 0;
				$json['message'] = "Error Haciendo el proceso de registro : " . $mysqli->error;
				echo json_encode($json);

			}
		}


		if($_GET['wsdl'] == 'InsertTroza'){

			
			if($_GET['id_web'] != 0){
				$Lsql = "UPDATE Trozas SET tro_arbol_id = " . $_GET['tro_arbol_id'] . " , tro_diametro = '". $_GET['tro_diametro'] . "' , tro_longitud = '" . $_GET['tro_longitud'] . "' , tro_estado = 1 , tro_fecha = '" . $_GET['tro_fecha'] . "' , tro_serial =  '". $_GET['tro_serial'] . "' , tro_usuario =  ". $_GET['tro_usuario'] . "  WHERE id = " . $_GET['id_web'];

			} else{
				$Lsql = "INSERT INTO Trozas (tro_arbol_id, tro_diametro, tro_longitud, tro_estado, tro_fecha, tro_serial, tro_usuario) VALUES (" . $_GET['tro_arbol_id'] . " , '". $_GET['tro_diametro'] . "' , '" . $_GET['tro_longitud'] . "' , 1, '". $_GET['tro_fecha'] ."' , '" . $_GET['tro_serial'] . "' , ". $_GET['tro_usuario'] . ") ";
			}

			$json = array();
			//echo $Lsql;
			if ($mysqli->query($Lsql) === TRUE) {
				$json['code'] = 1;
				if($_GET['id_web'] != 0){
					$json['id'] = $_GET['id_web'];
				}else{
					$json['id'] = $mysqli->insert_id;
				}
				$json['message'] = 'Exito';
				echo json_encode($json);
			} else {
				$json['code'] = 0;
				$json['id'] = 0;
				$json['message'] = "Error Haciendo el proceso de registro : " . $mysqli->error;
				echo json_encode($json);

			}
		}

		if($_GET['wsdl'] == 'Exportar'){
			$Lsql = "SELECT * FROM Info_arboles JOIN Sector ON sec_id = sector JOIN Lote ON Lot_id = lote JOIN USUARI ON USUARI_ConsInte__b = usuario_creador";
			$resultado = $mysqli->query($Lsql);
			header("Content-type: application/vnd.ms-excel; charset=utf-8");
    		header("Content-Disposition: attachment; filename=datos_Tkcut.xls");
			echo "<table border='1' >";
				echo "<thead>";
					echo "<tr>";
						echo "<th>N&uacute;mero de individuo</th>";
						echo "<th>Sector</th>";
						echo "<th>Lote</th>";
						echo "<th>Di&aacute;metro de fuste</th>";
						echo "<th>Tipo de di&aacute;metro</th>";
						echo "<th>Medida DAP desde CAP</th>";
						echo "<th>C&oacute;digo QR del individuo</th>";
						echo "<th>Fecha registro</th>";
						echo "<th>Usuario registrador</th>";
						echo "<th>IMEI</th>";
					echo "</tr>";
				echo "</thead>";
				echo "<tbody>";

			while ($key = $resultado->fetch_object()) {
				echo "<tr>";
						echo "<td>".$key->numero_arbol."</td>";
						echo "<td>".$key->sec_descripcion."</td>";
						echo "<td>".utf8_decode($key->Lot_descripcion)."</td>";
						echo "<td>".$key->diametro."</td>";
						echo "<td>".$key->tipo_diametro."</td>";
						echo "<td>&nbsp;".$key->cap."</td>";
						echo "<td>".$key->codigo_qr."</td>";
						echo "<td>".$key->fecha."</td>";
						echo "<td>".$key->USUARI_NOMBRE." ".$key->USUARI_APELLIDO."</td>";
						echo "<td>&nbsp;".$key->imei_celular."</td>";
					echo "</tr>";
			}
				echo "</tbody>";
			echo "</table>";
		}



		if($_GET['wsdl'] == 'Get_all_regs'){

			$Lsql = "SELECT * FROM Info_arboles JOIN Sector ON sec_id = sector JOIN Lote ON Lot_id = lote JOIN USUARI ON USUARI_ConsInte__b = usuario_creador WHERE Info_arboles.id NOT IN (SELECT rel_reg_id FROM relacion_equipos_registros WHERE rel_imei = '".$_GET['imei']."')";
			$resultado = $mysqli->query($Lsql);
   			$json = array();
   			$i = 0;
			while ($key = $resultado->fetch_object()) {
				$json[$i]['numero_arbol'] = $key->numero_arbol;
				$json[$i]['sector'] = utf8_encode($key->sec_descripcion);
				$json[$i]['lote'] = utf8_encode($key->Lot_descripcion);
				$json[$i]['diametro'] = $key->diametro;
				$json[$i]['tipo_diametro'] = $key->tipo_diametro;
				$json[$i]['cap'] = $key->cap;
				$json[$i]['codigo_qr'] = $key->codigo_qr;
				$json[$i]['calidad'] = $key->calidad;
				$json[$i]['fecha'] = $key->fecha;
				$json[$i]['usuario_creador'] = $key->USUARI_ConsInte__b;
				$json[$i]['id'] = $key->id;
				$json[$i]['imei_celular'] = $key->id;
				$Ysql = "INSERT INTO relacion_equipos_registros (rel_imei, rel_reg_id) VALUES ('".$_GET['imei']."' , ".$key->id.")";
				$mysqli->query($Ysql);

				$i++;
			}

			if($i > 0){
				$resultado = array('code' => 1, 'registros' => $json , 'numero_registros' => $i  );
			}else{
				$resultado = array('code' => 0, 'message' => 'No hay datos' );
			}

			echo json_encode($resultado);
		}

		if($_GET['wsdl'] == 'Get_all_regs_count'){

			$Lsql = "SELECT * FROM Info_arboles JOIN Sector ON sec_id = sector JOIN Lote ON Lot_id = lote JOIN USUARI ON USUARI_ConsInte__b = usuario_creador WHERE Info_arboles.id NOT IN (SELECT rel_reg_id FROM relacion_equipos_registros WHERE rel_imei = '".$_GET['imei']."')";
			$resultado = $mysqli->query($Lsql);
   			$i = $resultado->num_rows;
			$resultado = array('code' => 1, 'numero_registros' => $i  );
			echo json_encode($resultado);
		}


		if($_GET['wsdl'] == 'Get_all_regs_sin'){

			$Lsql = "SELECT * FROM Info_arboles JOIN Sector ON sec_id = sector JOIN Lote ON Lot_id = lote JOIN USUARI ON USUARI_ConsInte__b = usuario_creador ";

			$resultado = $mysqli->query($Lsql);
   			$json = array();
   			$i = 0;
			/*while ($key = $resultado->fetch_object()) {
				$json[$i]['numero_arbol'] = $key->numero_arbol;
				$json[$i]['sector'] = utf8_encode($key->sec_descripcion);
				$json[$i]['lote'] = utf8_encode($key->Lot_descripcion);
				$json[$i]['diametro'] = $key->diametro;
				$json[$i]['tipo_diametro'] = $key->tipo_diametro;
				$json[$i]['cap'] = $key->cap;
				$json[$i]['codigo_qr'] = $key->codigo_qr;
				$json[$i]['calidad'] = $key->calidad;
				$json[$i]['fecha'] = $key->fecha;
				$json[$i]['usuario_creador'] = $key->USUARI_ConsInte__b;
				$json[$i]['id'] = $key->id;
				$json[$i]['imei_celular'] = $key->imei_celular;
				
				

				$i++;
			}*/
			while($r = $resultado->fetch_assoc()) {
			    $json[] = $r;
			}

			if($resultado->num_rows > 0){
				$resultado = array('code' => 1, 'registros' => $json , 'numero_registros' => $i  );
			}else{
				$resultado = array('code' => 0, 'message' => 'No hay datos' );
			}

			echo json_encode($resultado);
		}

		if($_GET['wsdl'] == 'Get_all_regs_sin_count'){

			$Lsql = "SELECT * FROM Info_arboles JOIN Sector ON sec_id = sector JOIN Lote ON Lot_id = lote JOIN USUARI ON USUARI_ConsInte__b = usuario_creador ";
			$resultado = $mysqli->query($Lsql);
   			$i = $resultado->num_rows;
		
			$resultado = array('code' => 1,  'numero_registros' => $i  );
			echo json_encode($resultado);
		}


		if($_GET['wsdl'] == 'get_all_users'){
			$Lsql = "SELECT * FROM USUARI WHERE USUARI_ESTADO = 1";
			$resultado = $mysqli->query($Lsql);
			$json = array();
			$i = 0;
			while ($key = $resultado->fetch_object()) {
				$json[$i]['USUARI_ConsInte__b'] = $key->USUARI_ConsInte__b;
				$json[$i]['USUARI_CODIGO'] = $key->USUARI_CODIGO;
				$json[$i]['USUARI_PASSWORD'] = $key->USUARI_PASSWORD;
				$json[$i]['USUARI_FECHA_C'] = $key->USUARI_FECHA_C;
				$json[$i]['USUARI_NOMBRE'] = utf8_encode($key->USUARI_NOMBRE);
				$json[$i]['USUARI_APELLIDO'] = utf8_encode($key->USUARI_APELLIDO);
				$json[$i]['USUARI_IDENTIFICACION'] = $key->USUARI_IDENTIFICACION;
				$json[$i]['USUARI_ROL'] = $key->USUARI_ROL;
				$json[$i]['USUARI_ESTADO'] = $key->USUARI_ESTADO;
				$i++;
			}


			if($i > 0){
				$resultado = array('code' => 1, 'registros' => $json );
			}else{
				$resultado = array('code' => 0, 'message' => 'No hay datos' );
			}
			echo json_encode($resultado);

		}

	}


	function InsertarImei($imei){
		$Lsql = "SELECT * FROM Info_arboles";
		$resultado = $mysqli->query($Lsql);
		while ($key = $resultado->fetch_object()) {				
			$Ysql = "INSERT INTO relacion_equipos_registros (rel_imei, rel_reg_id) VALUES ('".$imei."' , ".$key->id.")";
			$mysqli->query($Ysql);
		}
	}
?>
