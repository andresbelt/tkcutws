<?php
ini_set('memory_limit', '1024M');
require_once dirname(__FILE__) . '/vendor/autoload.php';

include ("conexion.php");

if(isset($_POST['wsdl'])){
	
	$mysqli->query("SET NAMES 'utf8'");

	if($_POST['wsdl'] == 'InsertArbol'){

		if(empty($_POST['query']))
		{
			echo("You didn't select any boxes.");
			return;
		}else{

			$stream = $_POST['query'];

			if (file_exists("fichero.json")) {

				unlink("fichero.json");
				$fp = fopen("fichero.json", "w");
				fputs($fp, $stream );
				fclose($fp);

				$testfile = dirname(__FILE__) . '/fichero.json';
			} else {

				$fp = fopen("fichero.json", "w");
				fputs($fp, $stream );
				fclose($fp);

				$testfile = dirname(__FILE__) . '/fichero.json';
			}




			$listener = new \JsonStreamingParser\Listener\InMemoryListener();
			$stream = fopen($testfile, 'r');
			try {
				$parser = new \JsonStreamingParser\Parser($stream, $listener);
				$parser->parse();
				fclose($stream);
			} catch (Exception $e) {
				fclose($stream);
				throw $e;
			}
			$obj = $listener->getJson();


			$json = array();
			foreach($obj as $item) {

				$path = "uploads/".$item['codigo_qr'].".png";
				$image = $item['image'];

				$filename = "./uploads";

				if($image != '0')
				{

					if (!file_exists($filename)) {
						mkdir($filename, 0777);
						file_put_contents($path,base64_decode($image));


					}else{

						file_put_contents($path,base64_decode($image));


					}
				}
				if($item['id_web'] != 0){

			 //UPDATE info_arboles SET codigo_qr = 301 , sector = 1 , lote = 'Alpes 2006' , numero_arbol='S1L20N301' , diametro = 60.0 , altura = 24, anomalia = 'Descopado', tipo_diametro = 'DAP' , cap = 0 , calidad =  'Regular' , fecha_edicion = 2017-07-09 , editado =  1 , imei_celular='353420086802504' , arb_actualizado = 1 WHERE numero_arbol = 'S1L20N301';
			// [{"id":"203","code":1,"message":"UPDATE IGNORE info_arboles SET codigo_qr = 203 , sector = '1' , lote = '27' , numero_arbol = 'S1L27N203' , diametro = 20.6 , altura = '', anomalia = '', tipo_diametro = 'DAP' , cap = 0 , calidad =  'Bueno' , fecha_edicion = '2018-11-29' , editado =  1 , imei_celular ='867259026097513' , arb_actualizado = 1 WHERE codigo_qr = '203'"},{"code":0,"id":0,"message":"Error Haciendo el proceso de registro : INSERT INTO info_arboles (codigo_qr, sector, lote, numero_arbol, diametro, tipo_diametro, cap, calidad, estado, fecha, altura,anomalia,usuario_creador,imei_celular) VALUES ('532388' ,'1' , '20' , 'S1L20N532388' , '23.2' , 'CAP' , '7.38478935946394' , 'Bueno' , 1 , '2018-11-29' , '' ,'' , '1' , '867259026097513')"}]
					$Lsql = "UPDATE IGNORE info_arboles SET codigo_qr = " . $item['codigo_qr'] ." , sector = '"  . $item['sector_numerico']. "' , lote = '". $item['lote_numero']."' , numero_arbol = '". $item['numero_arbol'] . "' , diametro = " . $item['diametro'] . " , altura = '" . $item['altura'] . "', anomalia = '" . $item['anomalia'] . "', tipo_diametro = '" . $item['tipo_diametro'] . "' , cap = " . $item['cap'] . " , calidad =  '". $item['calidad'] . "' , fecha_edicion = '". $item['fecha'] . "' , editado =  ". $item['usuario_creador'] . " , imei_celular ='".$_POST['imei']."' , arb_actualizado = 1 WHERE codigo_qr = '" . $item['codigo_qr']."'";
				} else{


					$Lsql="INSERT INTO info_arboles (codigo_qr, sector, lote, numero_arbol, diametro, tipo_diametro, cap, calidad, estado, fecha, altura,anomalia,usuario_creador,imei_celular) VALUES ('" . $item['codigo_qr'] . "' ,'"  . $item['sector']. "' , '". $item['lote']. "' , '". $item['numero_arbol'] . "' , '" . $item['diametro'] . "' , '". $item['tipo_diametro'] ."' , '" . $item['cap'] . "' , '". $item['calidad'] . "' , 1 , '". $item['fecha'] . "' , '". $item['altura'] . "' ,'". $item['anomalia'] . "' , '". $item['usuario_creador'] ."' , '".$_POST['imei']."')";
				}



				$row = array();

				if ($mysqli->query($Lsql) === TRUE) {

					if($item['id_web'] != 0){
						$row['id'] = $item['codigo_qr'];

					}else{

						$row['id'] = $item['codigo_qr'];
					}

					$row['code'] = 1;

					$row['message'] = $Lsql;
					$json[] =$row ;

				}else{

					$row['code'] = 0;
					$row['id'] = 0;
					$row['id_web']=$item['id_web'];
					$row['message'] = "Error Haciendo el proceso de registro : " . $Lsql;
					$json[] =$row ;
				}


		


			}

			echo json_encode($json);
		}

	}

}


if(isset($_GET['wsdl'])){
	if($_GET['wsdl'] == 'VerImages'){

		$filename = "./uploads";

		if (!file_exists($filename)) {
			mkdir("./uploads", 0777);

			if ($handle = opendir('./uploads')) {

				while (false !== ($file = readdir($handle))) {
					if ($file != "." && $file != "..") {
						$colorList[]=$file;
					}

				}
				closedir($handle);
			}
			if($colorList === NULL) 
				echo json_encode([]);
			else
				echo json_encode($colorList);


		} else {



			if ($handle = opendir('./uploads')) {

				while (false !== ($file = readdir($handle))) {
					if ($file != "." && $file != "..") {
						$colorList[]=$file;
					}
				}
				closedir($handle);
			}else{

				$colorList['error'] = 1;
			}

			if($colorList === NULL) 
				echo json_encode([]);
			else
				echo json_encode($colorList);

		}

	}

		//http://192.168.0.11/Tkcut/service.php?wsdl=InsertArbol&codigo_qr=3453543&image=[B@f748977&sector=1&Lote=20&numero_arbol=S1L20N3453543&diametro=92.0&tipo_diametro=DAP&cap=0&calidad=Bueno&fecha=2017-07-08&Usuario=1&imei=353420086802504&id_web=

	if($_GET['wsdl'] == 'InsertTroza'){

		// if($_GET['tro_id_web'] == 'DELETE'){

		// 	$Lsql = "DELETE FROM trozas WHERE tro_serial = '" . $_GET['tro_serial'] . "'";

		// }else{

			$Lsql = "REPLACE trozas (tro_arbol_id, tro_diametro,tro_tipo_diametro, tro_cap, tro_longitud, tro_estado, tro_fecha, tro_serial, tro_usuario,tro_ubicacion,tro_calidadcom,tro_cortero,tro_id_web) VALUES ('" . $_GET['tro_arbol_id'] . "' , '". $_GET['tro_diametro'] . "' ,'". $_GET['tro_tipo_diametro'] . "' ,'". $_GET['tro_cap'] . "' , '" . $_GET['tro_longitud'] . "' , 1, '". $_GET['tro_fecha'] ."' , '" . $_GET['tro_serial'] . "' , '". $_GET['tro_usuario'] . "' , '". $_GET['tro_ubicacion'] . "' , '". $_GET['tro_calidadcom'] . "' , '". $_GET['tro_cortero'] . "', '". $_GET['tro_id_web'] . "') ";

		//}

		$json = array();
			//echo $Lsql;
		if ($mysqli->query($Lsql) === TRUE) {
			$json['code'] = 1;
			$json['id'] = $_GET['id'];
			$json['tro_diametro'] = $_GET['tro_diametro'];
			$json['tro_tipo_diametro'] = $_GET['tro_tipo_diametro'];
			$json['tro_cap'] = $_GET['tro_cap'];
			$json['tro_longitud'] = $_GET['tro_longitud'];
			$json['tro_fecha'] = $_GET['tro_fecha'];
			$json['tro_serial'] = $_GET['tro_serial'];
			$json['tro_usuario'] = $_GET['tro_usuario'];
			$json['tro_ubicacion'] = $_GET['tro_ubicacion'];
			$json['tro_calidadcom'] = $_GET['tro_calidadcom'];
			$json['tro_cortero'] = $_GET['tro_cortero'];
			$json['tro_id_web'] = $_GET['tro_id_web'];
			$json['message'] = 'Exito';
			echo json_encode($json);
		} else {
			$json['code'] = $Lsql;
			$json['id'] = 0;
			$json['message'] = "Error Haciendo el proceso de registro : " . $mysqli->error;
			echo json_encode($json);

		}
	}

	if($_GET['wsdl'] == 'get_trozas'){
		$Lsql = "SELECT * FROM trozas ORDER BY tro_arbol_id";
		$resultado = $mysqli->query($Lsql);
		$json = array();


		if($resultado->num_rows > 0){
			while($r = $resultado->fetch_assoc()) {
				$json[] = $r;
			}
			$resultado = array('code' => 1, 'registros' => $json , 'numero_registros' => $resultado->num_rows );
		}else{
			$resultado = array('code' => 0, 'message' => 'No hay datos' );
		}

		echo json_encode($resultado);
	}

	if($_GET['wsdl'] == 'Exportar'){
		$Lsql = "SELECT numero_arbol, sec_descripcion, Lot_descripcion,
		diametro, tipo_diametro, cap, codigo_qr, fecha , CONCAT(a.USUARI_NOMBRE, ' ', a.USUARI_APELLIDO) as Creador, fecha_edicion, CONCAT(b.USUARI_NOMBRE, ' ', b.USUARI_APELLIDO) as editor, imei_celular
		FROM info_arboles 
		JOIN Sector ON sec_id = sector 
		JOIN Lote ON Lot_id = lote 
		JOIN USUARI a ON a.USUARI_ConsInte__b = usuario_creador
		LEFT JOIN USUARI b ON b.USUARI_ConsInte__b = editado";


		$resultado = $mysqli->query($Lsql);
		header("Content-type: application/vnd.ms-excel; charset=utf-8");
		header("Content-Disposition: attachment; filename=datos_Tkcut.xls");
		echo "<table border='1' >";
		echo "<thead>";
		echo "<tr>";
		echo "<th>N&uacute;mero de individuo</th>";
		echo "<th>Sector</th>";
		echo "<th>Lote</th>";
		echo "<th>Di&aacute;metro de fuste</th>";
		echo "<th>Tipo de di&aacute;metro</th>";
		echo "<th>Medida DAP desde CAP</th>";
		echo "<th>C&oacute;digo QR del individuo</th>";
		echo "<th>Fecha registro</th>";
		echo "<th>Usuario que registro</th>";
		echo "<th>Fecha edici&oacute;n</th>";
		echo "<th>Usuario que edito</th>";
		echo "<th>IMEI</th>";
		echo "</tr>";
		echo "</thead>";
		echo "<tbody>";

		while ($key = $resultado->fetch_object()) {
			echo "<tr>";
			echo "<td>".$key->numero_arbol."</td>";
			echo "<td>".$key->sec_descripcion."</td>";
			echo "<td>".utf8_decode($key->Lot_descripcion)."</td>";
			echo "<td>".$key->diametro."</td>";
			echo "<td>".$key->tipo_diametro."</td>";
			echo "<td>&nbsp;".$key->cap."</td>";
			echo "<td>".$key->codigo_qr."</td>";
			echo "<td>".$key->fecha."</td>";
			echo "<td>".$key->Creador."</td>";
			echo "<td>".$key->fecha_edicion."</td>";
			echo "<td>".$key->editor."</td>";
			echo "<td>&nbsp;".$key->imei_celular."</td>";
			echo "</tr>";
		}
		echo "</tbody>";
		echo "</table>";
	}



	if($_GET['wsdl'] == 'Get_all_regs'){

		$Lsql = "SELECT * FROM info_arboles JOIN Sector ON sec_id = sector JOIN Lote ON Lot_id = lote JOIN USUARI ON USUARI_ConsInte__b = usuario_creador WHERE imei_celular != '".$_GET['imei']."' AND id > ". $_GET['id'] ." OR arb_actualizado = 1 ORDER BY id";
		$resultado = $mysqli->query($Lsql);
		$json = array();


		if($resultado->num_rows > 0){
			while($r = $resultado->fetch_assoc()) {
				$json[] = $r;
			}
			$resultado = array('code' => 1, 'registros' => $json , 'numero_registros' => $resultado->num_rows );
		}else{
			$resultado = array('code' => 0, 'message' => 'No hay datos' );
		}

		echo json_encode($resultado);
	}

	if($_GET['wsdl'] == 'Get_all_regs_count'){

		$Lsql = "SELECT * FROM info_arboles JOIN Sector ON sec_id = sector JOIN Lote ON Lot_id = lote JOIN USUARI ON USUARI_ConsInte__b = usuario_creador WHERE imei_celular != '".$_GET['imei']."' AND id > ". $_GET['id'];
		$resultado = $mysqli->query($Lsql);
		$i = $resultado->num_rows;
		$resultado = array('code' => 1 );
		echo json_encode($resultado);

	}

	//SELECT * FROM `info_arboles` WHERE `fecha` >= '201-06-05' LIMIT 0, 25

	if($_GET['wsdl'] == 'Get_all_regs_count_totaporfecha'){

		//	$Lsql = "SELECT * FROM info_arboles  JOIN Sector ON sec_id = sector JOIN Lote ON Lot_id = lote JOIN USUARI ON USUARI_ConsInte__b = usuario_creador ";
		$Lsql = "SELECT count(id) AS count FROM info_arboles WHERE `fecha` >= ".$_GET['fecha']."";
		$resultado = $mysqli->query($Lsql);
		$key = $resultado->fetch_object();
		$resultadojson = array('code' => 1, 'total' => $key->count );

	
		//header('Content-Length: ' . filesize('file.txt'));
		//readfile('file.txt');
		echo json_encode($resultadojson);
	}

	if($_GET['wsdl'] == 'Get_all_regs_ran'){

		//	$Lsql = "SELECT * FROM info_arboles JOIN Sector ON sec_id = sector JOIN Lote ON Lot_id = lote JOIN USUARI ON USUARI_ConsInte__b = usuario_creador ORDER BY id";
		//$Lsql = "SELECT * FROM `info_arboles` WHERE `codigo_qr` NOT BETWEEN ".$_GET['desde']." AND ".$_GET['hasta']."";
		//"SELECT * FROM `info_arboles` WHERE `fecha` >= ".$_GET['desde'].""
		// if($_GET['fecha'] == 0){
		// //$Lsql = "SELECT * FROM `info_arboles` WHERE `fecha` >= ".$_GET['desde']."";
		// $Lsql = "SELECT * FROM `info_arboles`";
		
		
		// }else{
		// //$Lsql = "SELECT * FROM `info_arboles` WHERE `fecha` >= '".$_GET['desde']."'";
		// $Lsql = "SELECT * FROM `info_arboles` WHERE `fecha` >= ".$_GET['fecha']." BETWEEN ".$_GET['desde']." AND ".$_GET['hasta']."";
		// 	}

			// if($resultado->num_rows > 0){
			// 	$resultado = array('code' => 1, 'registros' => $json , 'numero_registros' => $resultado->num_rows   );
			// }else{
			// 	$resultado = array('code' => 0, 'message' => 'No hay datos' );
			// }

			for($i=0 ; $i<4 ; $i++){
				$j= (($i*10000)-9999);
				$k= ($i*10000);
				$Lsql = "SELECT * FROM `info_arboles` WHERE `fecha` >= ".$_GET['fecha']." BETWEEN ".$j." AND ".$k."";
				$resultado = $mysqli->query($Lsql);
				$json = array();
		
				while($r = $resultado->fetch_assoc()) {
					$json[] = $r;
				}
		
			}



		
		if($resultado->num_rows > 0){
			$resultado = array('code' => 1, 'registros' => $json   );
		}else{
			$resultado = array('code' => 0, 'registros' => 'No hay datos' );
		}

		header('Content-Type: application/octet-stream');
		header('Content-Disposition: attachment; filename='.basename('file.txt'));
		header('Expires: 0');
		header('Cache-Control: must-revalidate');
		header('Pragma: public');

		//echo json_encode($resultado);
	}



	if($_GET['wsdl'] == 'Get_all_regs_sin'){

		//	$Lsql = "SELECT * FROM info_arboles JOIN Sector ON sec_id = sector JOIN Lote ON Lot_id = lote JOIN USUARI ON USUARI_ConsInte__b = usuario_creador ORDER BY id";
		$Lsql = "SELECT *, count(numero_arbol) FROM info_arboles JOIN Sector ON sec_id = sector JOIN Lote ON Lot_id = lote JOIN USUARI ON USUARI_ConsInte__b = usuario_creador group by numero_arbol having count(numero_arbol) >= 1 ORDER BY id";

		$resultado = $mysqli->query($Lsql);
		$json = array();

		while($r = $resultado->fetch_assoc()) {
			$json[] = $r;
		}

		if($resultado->num_rows > 0){
			$resultado = array('code' => 1, 'registros' => $json , 'numero_registros' => $resultado->num_rows   );
		}else{
			$resultado = array('code' => 0, 'message' => 'No hay datos' );
		}

		echo json_encode($resultado);
	}


	if($_GET['wsdl'] == 'Get_all_regs_sin_count'){

		//	$Lsql = "SELECT * FROM info_arboles  JOIN Sector ON sec_id = sector JOIN Lote ON Lot_id = lote JOIN USUARI ON USUARI_ConsInte__b = usuario_creador ";

		$Lsql = "SELECT *, count(numero_arbol) FROM info_arboles JOIN Sector ON sec_id = sector JOIN Lote ON Lot_id = lote JOIN USUARI ON USUARI_ConsInte__b = usuario_creador group by numero_arbol having count(numero_arbol) >= 1 ";
		$resultado = $mysqli->query($Lsql);
		$i = $resultado->num_rows;

		$resultado = array('code' => 1,  'numero_registros' => $i  );
		echo json_encode($resultado);
	}


	if($_GET['wsdl'] == 'get_all_users'){
		$Lsql = "SELECT * FROM usuari WHERE USUARI_ESTADO = 1";
		$resultado = $mysqli->query($Lsql);
		$json = array();
		$i = 0;
		while ($key = $resultado->fetch_object()) {
			$json[$i]['USUARI_ConsInte__b'] = $key->USUARI_ConsInte__b;
			$json[$i]['USUARI_CODIGO'] = $key->USUARI_CODIGO;
			$json[$i]['USUARI_PASSWORD'] = $key->USUARI_PASSWORD;
			$json[$i]['USUARI_FECHA_C'] = $key->USUARI_FECHA_C;
			$json[$i]['USUARI_NOMBRE'] = utf8_encode($key->USUARI_NOMBRE);
			$json[$i]['USUARI_APELLIDO'] = utf8_encode($key->USUARI_APELLIDO);
			$json[$i]['USUARI_IDENTIFICACION'] = $key->USUARI_IDENTIFICACION;
			$json[$i]['USUARI_ROL'] = $key->USUARI_ROL;
			$json[$i]['USUARI_ESTADO'] = $key->USUARI_ESTADO;
			$i++;
		}


		if($i > 0){
			$resultado = array('code' => 1, 'registros' => $json );
		}else{
			$resultado = array('code' => 0, 'message' => 'No hay datos' );
		}
		echo json_encode($resultado);

	}

	if($_GET['wsdl'] == "LlenarGrilla"){
            $page = $_POST['page'];  // Almacena el numero de pagina actual
            $limit = $_POST['rows']; // Almacena el numero de filas que se van a mostrar por pagina
            $sidx = $_POST['sidx'];  // Almacena el indice por el cual se hará la ordenación de los datos
            $sord = $_POST['sord'];  // Almacena el modo de ordenación
            if(!$sidx) $sidx =1;
            //Se hace una consulta para saber cuantos registros se van a mostrar
            $result = $mysqli->query("SELECT COUNT(*) AS count FROM info_arboles");
            // Se obtiene el resultado de la consulta
            $fila = $result->fetch_array();
            $count = $fila['count'];
            //En base al numero de registros se obtiene el numero de paginas
            if( $count >0 ) {
            	$total_pages = ceil($count/$limit);
            } else {
            	$total_pages = 0;
            }
            if ($page > $total_pages)
            	$page=$total_pages;

            //Almacena numero de registro donde se va a empezar a recuperar los registros para la pagina
            $start = $limit*$page - $limit; 
            //Consulta que devuelve los registros de una sola pagina

            $Lsql = "SELECT * FROM info_arboles JOIN Sector ON sec_id = sector JOIN Lote ON Lot_id = lote JOIN USUARI ON USUARI_ConsInte__b = usuario_creador ";

            if ($_REQUEST["_search"] == "false") {
            	$where = " where 1";
            } else {
            	$operations = array(
                    'eq' => "= '%s'",            // Equal
                    'ne' => "<> '%s'",           // Not equal
                    'lt' => "< '%s'",            // Less than
                    'le' => "<= '%s'",           // Less than or equal
                    'gt' => "> '%s'",            // Greater than
                    'ge' => ">= '%s'",           // Greater or equal
                    'bw' => "like '%s%%'",       // Begins With
                    'bn' => "not like '%s%%'",   // Does not begin with
                    'in' => "in ('%s')",         // In
                    'ni' => "not in ('%s')",     // Not in
                    'ew' => "like '%%%s'",       // Ends with
                    'en' => "not like '%%%s'",   // Does not end with
                    'cn' => "like '%%%s%%'",     // Contains
                    'nc' => "not like '%%%s%%'", // Does not contain
                    'nu' => "is null",           // Is null
                    'nn' => "is not null"        // Is not null
                    ); 
            	$value = $mysqli->real_escape_string($_REQUEST["searchString"]);
            	$where = sprintf(" where %s ".$operations[$_REQUEST["searchOper"]], $_REQUEST["searchField"], $value);
            }

            $Lsql .= $where.' ORDER BY '.$sidx.' '.$sord.' LIMIT '.$start.','.$limit;
            $result = $mysqli->query($Lsql);
           // echo $Lsql;
            $respuesta = array();
            $respuesta['page'] = $page;
            $respuesta['total'] = $total_pages;
            $respuesta['records'] = $count;
            $i=0;
            while( $fila = $result->fetch_object() ) {	
            	$respuesta['rows'][$i]['id']=$fila->id;
            	$respuesta['rows'][$i]['cell']=array($fila->id , $fila->numero_arbol, utf8_encode($fila->sec_descripcion) , $fila->Lot_descripcion , $fila->diametro , utf8_encode($fila->tipo_diametro) , $fila->cap , $fila->codigo_qr , utf8_encode($fila->fecha) , utf8_encode($fila->USUARI_NOMBRE." ".$fila->USUARI_APELLIDO));
            	$i++;
            }

            // La respuesta se regresa como json
            echo json_encode($respuesta);
        }

        if($_GET['wsdl'] == "CargarSectores"){
        	$Lsql = "SELECT * FROM sector";
        	$resultado = $mysqli->query($Lsql);
        	echo '<select class="form-control"  name="sector" id="sector">';
        	while($obj = $resultado->fetch_object()){
        		echo "<option value='".$obj->sec_id."'>".utf8_encode($obj->sec_descripcion)."</option>";
        	}   
        	echo '</select>'; 
        }

        if($_GET['wsdl'] == "CargarLotes"){
        	$sector = 'I';
        	switch ($_GET['sector']) {
        		case 1:
        		$sector = "I";
        		break;
        		case 2:
        		$sector = "II";
        		break;
        		case 3:
        		$sector = "III";
        		break;
        		case 4:
        		$sector = "IV";
        		break;
        		case 5:
        		$sector = "V";
        		break;
        		case 6:
        		$sector = "VI";
        		break;
        		case 7:
        		$sector = "VII";
        		break;

        		default:
        		$sector = "I";
        		break;
        	}
        	$Lsql = "SELECT * FROM lote WHERE Lot_sector='".$sector."'";
        	//echo $Lsql;
        	$resultado = $mysqli->query($Lsql);
        	echo '<select class="form-control"  name="Lote" id="Lote">';
        	while($obj = $resultado->fetch_object()){
        		echo "<option value='".$obj->Lot_id."'>".utf8_encode($obj->Lot_descripcion)."</option>";
        	}   
        	echo '</select>'; 
        }

        if($_GET['wsdl'] == "CargarTipoDiametro"){
        	echo '<select class="form-control"  name="tipo_diametro" id="tipo_diametro">';
        	echo '<option value="DAP">DAP</option>';
        	echo "<option value='CAP'>CAP</option>";
        	echo '</select>'; 
        }

    }

    ?>
