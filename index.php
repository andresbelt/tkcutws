<?php
    session_start();
    if(!isset($_SESSION['LOGIN_OK'])){
        header('Location: login.php');
    }
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Tkcut - Sistema de inventario forestal</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.6 -->
        <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="assets/font-awesome/css/font-awesome.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="assets/ionicons-master/css/ionicons.min.css">

        
        <!-- Theme style -->
        <link rel="stylesheet" href="assets/css/AdminLTE.min.css">
        <!-- AdminLTE Skins. Choose a skin from the css/skins
        folder instead of downloading all of them to reduce the load. -->
        <link rel="stylesheet" href="assets/css/skins/_all-skins.min.css">
        <!-- iCheck -->
        <link rel="stylesheet" href="assets/plugins/iCheck/flat/blue.css">
        <!-- Morris chart -->
        <link rel="stylesheet" href="assets/plugins/morris/morris.css">
        <!-- jvectormap -->
        <link rel="stylesheet" href="assets/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
        <!-- Date Picker -->
        <link rel="stylesheet" href="assets/plugins/datepicker/datepicker3.css">
        <!-- Daterange picker -->
        <link rel="stylesheet" href="assets/plugins/daterangepicker/daterangepicker.css">
         <!-- Bootstrap time Picker -->
        <link rel="stylesheet" href="assets/plugins/timepicker/bootstrap-timepicker.min.css">
        <!-- bootstrap wysihtml5 - text editor -->
        <link rel="stylesheet" href="assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
        <link rel="stylesheet" href="assets/css/alertify.core.css">

        <link rel="stylesheet" href="assets/css/alertify.default.css">
        <link rel="stylesheet" type="text/css" media="screen" href="assets/Guriddo_jqGrid_/css/ui.jqgrid-bootstrap.css" />
        <link rel="stylesheet" href="assets/plugins/select2/select2.min.css" />
        <link rel="stylesheet" href="assets/plugins/sweetalert/sweetalert.css" />
        <script src="assets/plugins/jQuery/jquery-2.2.3.min.js"></script>
    </head>


    <body class="skin-blue layout-top-nav layout-boxed ">
        <div class="wrapper">
            <header class="main-header">
                <nav class="navbar navbar-static-top">
                    <div class="container">
                        <div class="navbar-header">
                            <a href="#" class="navbar-brand" id="hrefTitle">TKcut</a>
                            
                        </div>
                        <!-- Collect the nav links, forms, and other content for toggling -->
                        <div class="collapse navbar-collapse" id="navbar-collapse">
                            <ul class="nav navbar-nav" style="  visibility: hidden">
                            <li class="active"><a href="#">Link <span class="sr-only">(current)</span></a></li>
                            <li><a href="#">Link</a></li>
                            <li class="dropdown">
                              <a href="#" class="dropdown-toggle" data-toggle="dropdown">Dropdown <span class="caret"></span></a>
                              <ul class="dropdown-menu" role="menu">
                                <li><a href="#">Action</a></li>
                                <li><a href="#">Another action</a></li>
                                <li><a href="#">Something else here</a></li>
                                <li class="divider"></li>
                                <li><a href="#">Separated link</a></li>
                                <li class="divider"></li>
                                <li><a href="#">One more separated link</a></li>
                              </ul>
                            </li>
                          </ul>
                            <div class="navbar-custom-menu">
                            <ul class="nav navbar-nav">
                                <!-- User Account Menu -->
                                <li class="dropdown user user-menu">
                                    <!-- Menu Toggle Button -->
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                        <!-- The user image in the navbar-->
                                        <img src="assets/img/user2-160x160.jpg" class="user-image" alt="User Image">
                                        <!-- hidden-xs hides the username on small devices so only the image appears. -->
                                        <span class="hidden-xs">
                                            <?php  echo $_SESSION['NOMBRES'] ; ?>
                                        </span>
                                    </a>
                                    <ul class="dropdown-menu">
                                        <!-- The user image in the menu -->
                                        <li class="user-header">
                                            <img src="assets/img/user2-160x160.jpg" class="img-circle" alt="User Image">
                                            <p>
                                                <?php echo $_SESSION['NOMBRES'] ; ?>
                                                <?php 
                                                    if($_SESSION['FECHA'] != '' && $_SESSION['FECHA'] != NULL){
                                                        $fecha = $_SESSION['FECHA'];
                                                        $meses = array('Ene', 'Feb', 'marzo', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic');
                                                        $otra = explode('-',$fecha);
                                                        $num = $otra[1] - 1;
                                                       
                                                        echo "<small>Miembro desde ". $meses[$num]." ". $otra[0]."</small>";
                                                    }
                                                        
                                                ?>
                                            </p>
                                        </li>
                                        <!-- Menu Body -->
                                        <li class="user-body">
                                            <div class="row">
                                                <div class="col-xs-6 text-center">
                                                    <a href="#" class="btn btn-default btn-flat">Cambiar Contraseña</a>
                                                </div>
                                                <div class="col-xs-6 text-center">
                                                    <a href="salir.php" class="btn btn-default btn-flat">Sign out</a>
                                                </div>
                                            </div>
                                        <!-- /.row -->
                                        </li>
                        
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <!-- /.navbar-custom-menu -->
                        </div><!-- /.navbar-collapse -->
                        <!-- Navbar Right Menu -->
                        
                  </div>
                  <!-- /.container-fluid -->
                </nav>
            </header>
            
            <div class="content-wrapper">   
                <section class="content-header">
                    <h1>
                        Registros
                    </h1>
                    <ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-edit"></i> Registros </a></li>
                    <li><a href="service.php?wsdl=Exportar"><i class="fa fa-edit"></i> Exportar a Excel </a></li>
                    </ol>
                </section>


                <!-- Main content -->
                <section class="content">

                    <div class="row">
                        <div class="col-md-12">
                            <div class="box box-success">
                                <div class="box-body">
                                    <div class="table-responsive" id="tablaAgentes">
                                        <table id="tablaDatos" class="table table-hover table-bordered" width="100%">
                                        </table>
                                        <div id="pager">
                                        </div> 
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                        
            </div><!-- /.page-content -->
        </div>
        <!-- ./COntent wrapper -->


        <!-- jQuery 2.2.3 -->
        
        <!-- jQuery UI 1.11.4 -->
        <script src="assets/jqueryUI/jquery-ui.min.js"></script>
        <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
        <script>
          $.widget.bridge('uibutton', $.ui.button);
        </script>
        <!-- Bootstrap 3.3.6 -->
        <script src="assets/bootstrap/js/bootstrap.min.js"></script>

        <script src="assets/plugins/select2/select2.full.min.js"></script>
        <!-- Sparkline -->
        <script src="assets/plugins/sparkline/jquery.sparkline.min.js"></script>
        <!-- Slimscroll -->
        <script src="assets/plugins/slimScroll/jquery.slimscroll.min.js"></script>


        <!-- daterangepicker -->
        <script src="assets/js/moment.min.js"></script>
        <script src="assets/plugins/daterangepicker/daterangepicker.js"></script>


        <!-- datepicker -->
        <script src="assets/plugins/datepicker/bootstrap-datepicker.js"></script>
        <script src="assets/plugins/timepicker/bootstrap-timepicker.min.js"></script>
        <!-- FastClick -->
        <script src="assets/plugins/fastclick/fastclick.js"></script>

        <link rel="stylesheet" href="assets/plugins/timepicker/bootstrap-timepicker.min.css">
        <script src="assets/plugins/timepicker/bootstrap-timepicker.min.js"></script>

        <script src="assets/Guriddo_jqGrid_/js/i18n/grid.locale-es.js" type="text/javascript"></script>
        <script src="assets/Guriddo_jqGrid_/js/jquery.jqGrid.min.js" type="text/javascript"></script>

        <script src="assets/plugins/sweetalert/sweetalert.min.js"></script>      
        
        
        <!-- AdminLTE App -->
        <script src="assets/js/app.min.js"></script>
        <script src="assets/js/numeric.js"></script>
        <script src="assets/js/alertify.js"></script>


        <script type="text/javascript">
            $(function(){
    

                $.fn.datepicker.dates['es'] = {
                    days: ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"],
                    daysShort: ["Dom", "Lun", "Mar", "Mie", "Jue", "Vie", "Sab"],
                    daysMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
                    months: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
                    monthsShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
                    today: "Today",
                    clear: "Clear",
                    format: "yyyy-mm-dd",
                    titleFormat: "yyyy-mm-dd", 
                    weekStart: 0
                };
                


                $(".FechaEntreValida").datepicker({
                    language: "es",
                    autoclose: true,
                    todayHighlight: true
                });


                $(".Numerico").numeric();
                $(".NumericoValidador").numeric();
                $(".Decimal").numeric({ decimal : ".",  negative : false, scale: 4 });


                <?php 

                    if(isset($_GET['result'])){ 
                        if($_GET['result'] == '1'){ ?>
                            swal({
                                title: "Exito!",
                                html : true,
                                text: 'El formulario a sido generado con exito!',
                                type: "success",
                                confirmButtonText: "Ok"
                            });
                <?php 
                        }
                    }

                ?>

                cargar_hoja_datos();
            });


            function cargar_hoja_datos(){
                $.jgrid.defaults.width = '1225';
                $.jgrid.defaults.height = '650';
                $.jgrid.defaults.responsive = true;
                $.jgrid.defaults.styleUI = 'Bootstrap';
                var lastsel2 = 0;
                $("#tablaDatos").jqGrid({
                    url:'service.php?wsdl=LlenarGrilla',
                    datatype: 'json',
                    mtype: 'POST',
                    colNames:[  'id',
                                '# Individuo',
                                'Sector',
                                'Lote',
                                'Di&aacute;metro de fuste',
                                'Tipo de di&aacute;metro',
                                'Medida DAP desde CAP',
                                'C&oacute;digo QR del individuo',
                                'Fecha registro',
                                'Usuario que registro'
                            ],
                    colModel:[
                                  //Traigo los datos de la base de dtaos y los defino en que columna va cada uno, tambien definimos con es su forma de edicion, sea Tipo listas, tipo Textos, etc.
                        {
                            name:'providerUserId',
                            index:'providerUserId', 
                            
                            editable:true, 
                            editrules:{
                                required:false, 
                                edithidden:true
                            },
                            hidden:true, 
                            editoptions:{ 
                                dataInit: function(element) {                     
                                  $(element).attr("readonly", "readonly"); 
                                } 
                            }
                        }

                        ,
                        { 
                            name:'numero_arbol', 
                            index: 'numero_arbol', 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'sector', 
                            index:'sector', 
                            editable: true, 
                            edittype:"select" , 
                            editoptions: {
                                dataUrl: 'service.php?wsdl=CargarSectores',
                                dataInit : function(el){
                                    $(el).change(function(){
                                        //alert("Hola "+ lastsel2);
                                        $.ajax({
                                            url  : "service.php?wsdl=CargarLotes&sector="+$(this).val(),
                                            type : "get",
                                            success : function(data){
                                                $("#"+ lastsel2 +"_Lote").html(data);
                                            }
                                        });
                                    });
                                }

                            }
                        }
         
                        ,
                        {  
                            name:'Lote', 
                            index:'Lote', 
                            editable: true,
                            edittype: "select",
                            editoptions: {
                                dataUrl: 'service.php?wsdl=CargarLotes&sector=1',
                                dataInit : function(el){
                                    $(el).change(function(){
                                      // var lote = 
                                    });
                                }
                            }
                        }

                        ,
                        {  
                            name:'diametro', 
                            index:'diametro', 
                            editable: true, 
                            searchoptions: {
                                sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                            }, 
                            editoptions:{
                                size:20,
                                dataInit:function(el){
                                    $(el).numeric({ decimal : ".",  negative : false, scale: 1 });
                                }
                            } 
                        }

                        ,
                        {  
                            name:'tipo_diametro', 
                            index:'tipo_diametro', 
                            editable: true ,
                            formatter: 'text', 
                            searchoptions: {
                                sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                            }, 
                            edittype:"select" , 
                            editoptions: {
                                dataUrl: 'service.php?wsdl=CargarTipoDiametro'
                            }
                        }

                        ,
                        {  
                            name:'cap', 
                            index:'cap', 
                            editable: true ,
                            formatter: 'text', 
                            searchoptions: {
                                sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                            }, 
                            editoptions:{
                                size:20,
                                dataInit:function(el){
                                    $(el).numeric({ decimal : ".",  negative : false, scale: 1 });
                                }
                            } 
                        }

                        ,
                        {   
                            name:'codigo_qr', 
                            index:'codigo_qr', 
                            editable: true, 
                            editoptions:{
                                size:20,
                                dataInit:function(el){
                                    $(el).numeric();
                                }
                            } 

                        }

                        ,
                        { 
                            name:'fecha', 
                            index:'fecha', 
                            editable: true, 
                            formatter: 'text', 
                            searchoptions: {
                                sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                            }, 
                            editoptions:{
                                size:20,
                                dataInit:function(el){
                                    $(el).datepicker({
                                        language: "es",
                                        autoclose: true,
                                        todayHighlight: true
                                    });
                                },
                                defaultValue: function(){
                                    var currentTime = new Date();
                                    var month = parseInt(currentTime.getMonth() + 1);
                                    month = month <= 9 ? "0"+month : month;
                                    var day = currentTime.getDate();
                                    day = day <= 9 ? "0"+day : day;
                                    var year = currentTime.getFullYear();
                                    return year+"-"+month + "-"+day;
                                }
                            }
                        }

                        ,
                        {   
                            name:'Usuario', 
                            index:'Usuario', 
                           
                        }
                    ],
                    beforeSelectRow: function(rowid){
                        if(rowid && rowid!==lastsel2){
                            $("#"+ rowid +"_sector").change(function(){
                                alert("Hola");
                                $.ajax({
                                    url  : "service.php?wsdl=CargarLotes&sector="+$(this).val(),
                                    type : "get",
                                    success : function(data){
                                        $("#"+ rowid +"_Lote").html(data);
                                    }
                                });
                            });
                        }
                        lastsel2=rowid;
                    },
                    pager: "#pager" ,
                    rowNum: 50,
                    rowList:[50,100],
                    loadonce: false,
                    sortable: true,
                    sortname: 'numero_arbol',
                    sortorder: 'desc',
                    viewrecords: true,
                    editurl:"service.php",
                    autowidth: true                    
                });

                $('#tablaDatos').navGrid("#pager", { add:false, del: false , edit: false });
                $('#tablaDatos').inlineNav('#pager',
                // the buttons to appear on the toolbar of the grid
                { 
                    edit: false, 
                    add: false, 
                    cancel: true,
                    editParams: {
                        keys: true,
                    },
                    addParams: {
                        keys: true
                    }
                });
              
                        //para cuando se Maximice o minimize la pantalla.
                $(window).bind('resize', function() {
                    $("#tablaDatos").setGridWidth($(window).width());
                }).trigger('resize'); 
            }
        </script>
    </body>
</html>
